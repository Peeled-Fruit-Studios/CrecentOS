#include <hal.h>
#include <kd.h>

extern UINT64 __interrupt_vector[];
static struct IDT_ENTRY IDT[256];

// start allocating irq's at 64
static UINT8 lastvector = 64;

static struct IDT_ENTRY HalpMakeEntry(IN UINT64 offset) {
    return (struct IDT_ENTRY) {
        .selector = CODE_SEGMENT_SELECTOR,
        .offset_15_0 = offset & 0xFFFF,
        .offset_31_16 = (offset >> 16) & 0xFFFF,
        .offset_63_32 = (offset >> 32) & 0xFFFFFFFF,
        .flags = IDT_FLAGS_DEFAULT
    };
}

void HalpSetHandler(IN UINT8 vector, void* handler) {
    IDT[vector] = HalpMakeEntry((UINT64)handler);
}

UINT8 HalpGetVector() {
    lastvector++;
    if (lastvector == 0)
        INFO("Out of IRQ vectors\n");

    return lastvector;
}

VOID
HalpInitIDT(){
	for(int i = 0; i < 31; i++)
		IDT[i] = HalpMakeEntry((UINT64)__interrupt_vector[i]);

    struct IDTR g = { .limit = sizeof(IDT) - 1, .base = (UINT64)&IDT };
	
	__lidt(&g);
}

// ISR Stuff --------------------------------------

static const char *_exception_messages[32] = {
  "Division by zero",
  "Debug",
  "Non-maskable interrupt",
  "Breakpoint",
  "Detected overflow",
  "Out-of-bounds",
  "Invalid opcode",
  "No coprocessor",
  "Double fault",
  "Coprocessor segment overrun",
  "Bad TSS",
  "Segment not present",
  "Stack fault",
  "General protection fault",
  "Page fault",
  "Unknown interrupt",
  "Coprocessor fault",
  "Alignment check",
  "Machine check",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
  "Reserved",
};

void HalpHandleInterrupt(struct CpuState* ctx) {
	if(ctx->intno < 32) {
		LOG(LOG_FATAL, "A Fatal Error has been encountered with stopcode %x\n", ctx->intno);
		for(;;);
	} else {
		for(;;);
	}
}
