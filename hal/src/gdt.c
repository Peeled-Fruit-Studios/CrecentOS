#include <hal.h>

extern VOID HalpLoadGDT(void* gdt);

GDT gdt;

VOID 
HalpInitGDT() {
    //gdt = (GDT*)MmAllocatePool(sizeof(GDT), TAG_HAL);
    gdt = (GDT) {
        .entry_null = GDT_ENTRY_NULL,
        .entry_kcode = GDT_ENTRY_KERNEL_CODE,
        .entry_kdata = GDT_ENTRY_KERNEL_DATA,
        .entry_ucode = GDT_ENTRY_USER_CODE,
        .entry_udata = GDT_ENTRY_USER_DATA
    };

    struct GDTR g = { .base = (UINT64)&gdt, .limit = sizeof(GDT) - 1};
	
	HalpLoadGDT(&g);
}
