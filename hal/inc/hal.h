#pragma once

#include <cxkrnl.h>

#define TAG_HAL ' laH'

// GDT Code ----------------------------------------------------------------

#define GDT_ENTRY_NULL 0x0000000000000000
#define GDT_ENTRY_KERNEL_CODE 0x00AF9A000000FFFF
#define GDT_ENTRY_KERNEL_DATA 0x008F92000000FFFF
#define GDT_ENTRY_USER_CODE 0x00AFFA000000FFFF
#define GDT_ENTRY_USER_DATA 0x008FF2000000FFFF

#ifndef _MSC_VER
#define _pack __attribute__((packed))
#else
#define _pack
#pragma pack(push)
#pragma pack(1)
#endif


typedef struct {
    UINT16 seg_limit_0_15;
    UINT16 base_addr_0_15;
    UINT64 base_addr_16_23;
    UINT64 flags_low;
    UINT64 flags_high;
    UINT64 base_addr_24_31;
    UINT32 base_addr_32_63;
    UINT32 reserved;
} _pack TSS;

typedef struct {
    UINT64 entry_null;
    UINT64 entry_kcode;
    UINT64 entry_kdata;
    UINT64 entry_ucode;
    UINT64 entry_udata;
    TSS tss;
} _pack GDT;

struct _pack GDTR {
    UINT16 limit;
    UINT64 base;
};

VOID 
HalpInitGDT();

// IDT Code ------------------------------------------------------------------------

#pragma once

#define CODE_SEGMENT_SELECTOR 0x08

#define IDT_FLAGS_DEFAULT 0b1000111000000000

struct _pack IDT_ENTRY {
    UINT16 offset_15_0;
    UINT16 selector;
    UINT16 flags;
    UINT16 offset_31_16;
    UINT32 offset_63_32;
    UINT32 reserved;
};

struct _pack IDTR {
    UINT16 limit;
    UINT64 base;
};

_pack struct CpuState {
  UINT64 r15;
  UINT64 r14;
  UINT64 r13;
  UINT64 r12;
  UINT64 r11;
  UINT64 r10;
  UINT64 r9;
  UINT64 r8;
  UINT64 rbp;
  UINT64 rdi;
  UINT64 rsi;
  UINT64 rdx;
  UINT64 rcx;
  UINT64 rbx;
  UINT64 rax;

  UINT64 intno;
  UINT64 err;

  // the interrupt stackframe
  UINT64 rip;
  UINT64 cs;
  UINT64 rflags;
  UINT64 rsp;
  UINT64 ss;
};

#pragma pack(pop)

VOID
HalpInitIDT();

VOID
HalSetHandler(IN UINT8 vector, IN void* handler);

UINT8 HalpGetVector();
