;------------------------------------------------------------------------------
;
; Copyright (c) 2006 - 2008, Intel Corporation. All rights reserved.<BR>
; SPDX-License-Identifier: BSD-2-Clause-Patent
;
; Module Name:
;
;   CpuId.Asm
;
; Abstract:
;
;   AsmCpuid function
;
; Notes:
;
;------------------------------------------------------------------------------

    DEFAULT REL
    SECTION .text

;------------------------------------------------------------------------------
;  VOID
;  EFIAPI
;  AsmCpuid (
;    IN   UINT32  RegisterInEax,
;    OUT  UINT32  *RegisterOutEax  OPTIONAL,
;    OUT  UINT32  *RegisterOutEbx  OPTIONAL,
;    OUT  UINT32  *RegisterOutEcx  OPTIONAL,
;    OUT  UINT32  *RegisterOutEdx  OPTIONAL
;    )
;------------------------------------------------------------------------------
global AsmCpuid
AsmCpuid:
    push    rbx
    mov     eax, ecx
    push    rax                         ; save Index on stack
    push    rdx
    cpuid
    test    r9, r9
    jz      .0
    mov     [r9], ecx
.0:
    pop     rcx
    jrcxz   .1
    mov     [rcx], eax
.1:
    mov     rcx, r8
    jrcxz   .2
    mov     [rcx], ebx
.2:
    mov     rcx, [rsp + 0x38]
    jrcxz   .3
    mov     [rcx], edx
.3:
    pop     rax                         ; restore Index to rax as return value
    pop     rbx
    ret

;------------------------------------------------------------------------------
; VOID
; EFIAPI
; CpuBreakpoint (
;  VOID
;   )
;------------------------------------------------------------------------------

global CpuBreakpoint
CpuBreakpoint:
  push    rbx
  
  int 3 ; Trigger a Debug Interrupt

  pop     rbx
  ret

global AsmWriteCr0
AsmWriteCr0:
  mov     cr0, rcx
  mov     rax, rcx
  ret

global AsmReadCr0
AsmReadCr0:
  mov rax, cr0
  ret

global AsmReadCr3
AsmReadCr3:
  mov rax, cr3
  ret

