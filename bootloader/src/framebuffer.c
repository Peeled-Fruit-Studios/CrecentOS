#include <framebuffer.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Protocol/GraphicsOutput.h>

FrameBuffer MainBuf;

FrameBuffer* GetGOPInfo() {
  EFI_GRAPHICS_OUTPUT_PROTOCOL* gop = NULL;
  EFI_STATUS status;

  status = gBS->LocateProtocol(&gEfiGraphicsOutputProtocolGuid, NULL, (VOID**)&gop);
  if(EFI_ERROR(status)){
    Print(L"Graphics information (Required For Startup) is Unavailable.\n\r");
    return NULL;
  }

  MainBuf.Base = (void*)gop->Mode->FrameBufferBase;
  MainBuf.BSize = gop->Mode->FrameBufferSize;
  MainBuf.Width = gop->Mode->Info->HorizontalResolution;
  MainBuf.Height = gop->Mode->Info->VerticalResolution;
  MainBuf.Pitch = gop->Mode->Info->PixelsPerScanLine;

  return &MainBuf;
}

