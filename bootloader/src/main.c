#include <Uefi.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Library/UefiRuntimeServicesTableLib.h>

#include <modules.h>
#include <cmdline.h>
#include <framebuffer.h>

EFI_STATUS EFIAPI EfiMain(IN EFI_HANDLE ImageHandle, IN EFI_SYSTEM_TABLE *SystemTable) {
    EFI_STATUS Status = EFI_SUCCESS;

    DxeDebugLibConstructor(ImageHandle, SystemTable);
    UefiBootServicesTableLibConstructor(ImageHandle, SystemTable);
    UefiRuntimeServicesTableLibConstructor(ImageHandle, SystemTable);

    gST->BootServices->SetWatchdogTimer(0, 0, 0, NULL);

    // just a signature that we booted
    gST->ConOut->ClearScreen(gST->ConOut);
    Print(L"Cxldr (v0.1.3) (x86_64)\n\r");
    Print(L"Copyright (c) 2021 Peeled Fruit Studios, All Rights Reserved.\n\r\n");

    InitCmdline(ImageHandle, SystemTable);   

    // Print System information (for debug purposes)    
    if(GetIntCmdline("boot.logdbg", 1)) {
      Print(L"\nSystem Information\n\r");
      Print(L"== Vendor: %s (Revision %08x)\n\r", gST->FirmwareVendor, gST->FirmwareRevision);
      Print(L"== UEFI Version: %d.%d\n\r", (gST->Hdr.Revision >> 16u) & 0xFFFFu, gST->Hdr.Revision & 0xFFFFu);
      UINT32 eax, ebx, ecx, edx;
      AsmCpuidEx(0x00000007, 0, &eax, &ebx, &ecx, &edx);
      if (ecx & BIT16) {
        Print(L"== 5-Level Paging Supported!\n\r");
      }
    }

    // Get Graphics Info
    FrameBuffer* outbuf = GetGOPInfo();
    if(GetIntCmdline("boot.logdbg", 1)) {
      Print(L"== Resolution: %ux%u\n\r", outbuf->Width, outbuf->Height);
    }

    // Load the Kernel
    LoadKernel(ImageHandle, SystemTable);

    // Infinite Loop for now...
    for(;;);

    return Status;
}
