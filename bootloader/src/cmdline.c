#include <cmdline.h>
#include <modules.h>

#define CMDLINE_MAX_ITEMS 128
#define CMDLINE_MAX_STRINGDATA (1024 * 3)

static char buffer[CMDLINE_MAX_STRINGDATA];
static UINTN buffer_next = 0;

typedef struct {
    char* key;
    char* val;
    UINTN klen;
    UINTN vlen;
} kv_t;

static kv_t entry[CMDLINE_MAX_ITEMS];
static UINTN entry_count;

static char* RawCmd = NULL;

char* GetRawCmd() {
  return RawCmd;
}

UINTN cmdline_to_string(char* ptr, UINTN max) {
    char* start = ptr;

    if (max == 0) {
        return 0;
    }

    for (UINTN n = 0; n < entry_count; n++) {
        if ((entry[n].klen + entry[n].vlen + 3) > max) {
            // require space for: space + key + equal + value + null
            break;
        }
        if (n > 0) {
            *ptr++ = ' ';
            max--;
        }
        CopyMem(ptr, entry[n].key, entry[n].klen);
        ptr += entry[n].klen;
        max -= entry[n].klen;
        if (entry[n].vlen) {
            *ptr++ = '=';
            max--;
            CopyMem(ptr, entry[n].val, entry[n].vlen);
            ptr += entry[n].vlen;
            max -= entry[n].vlen;
        }
    }
    *ptr++ = 0;
    return ptr - start;
}

static int isspace(int c) {
   return c == ' ' || c == '\t'; // || whatever other char you consider space
}

static unsigned int strlen(const char *s) {
    unsigned int count = 0;
    while(*s!='\0')
    {
        count++;
        s++;
    }
    return count;
}

static long int atol(char* string) {
    long int result = 0;
    unsigned int digit;
    int sign;

    /*
     * Skip any leading blanks.
     */

    while (isspace(*string)) {
	string += 1;
    }

    /*
     * Check for a sign.
     */

    if (*string == '-') {
	sign = 1;
	string += 1;
    } else {
	sign = 0;
	if (*string == '+') {
	    string += 1;
	}
    }

    for ( ; ; string += 1) {
	digit = *string - '0';
	if (digit > 9) {
	    break;
	}
	result = (10*result) + digit;
    }

    if (sign) {
	return -result;
    }
    return result;
}

static void AddEntry(const char* key, UINTN klen, const char* val, UINTN vlen) {
    if (klen == 0) {
        // empty keys are not allowed
        return;
    }

    if ((klen > 1024) || (vlen > 1024)) {
        // huge keys and values are not allowed
        return;
    }

    if ((sizeof(buffer) - buffer_next) < (klen + vlen + 2)) {
        // give up if it won't fit
        return;
    }

    UINTN n;
    for (n = 0; n < entry_count; n++) {
        if ((entry[n].klen == klen) && !CompareMem(key, entry[n].key, klen)) {
            goto write_value;
        }
    }
    if (n == CMDLINE_MAX_ITEMS) {
        // no space in table
        return;
    }

    // new entry
    entry_count++;
    entry[n].key = buffer + buffer_next;
    entry[n].klen = klen;
    CopyMem(entry[n].key, key, klen);
    entry[n].key[klen] = 0;
    buffer_next += klen + 1;

write_value:
    entry[n].val = buffer + buffer_next;
    entry[n].vlen = vlen;
    CopyMem(entry[n].val, val, vlen);
    entry[n].val[vlen] = 0;
    buffer_next += vlen + 1;
}

void SetCmdline(const char* key, const char* val) {
    AddEntry(key, strlen(key), val, strlen(val));
}

void AppendCmdline(const char* str, UINTN len) {
    const char* key;
    const char* val;

restart:
    while (len > 0) {
        if (isspace(*str)) {
            str++;
            len--;
            continue;
        }
        key = str;
        while (len > 0) {
            if (*str == '=') {
                UINTN klen = str - key;
                str++;
                len--;
                val = str;
                while ((len > 0) && !isspace(*str)) {
                    len--;
                    str++;
                }
                UINTN vlen = str - val;
                AddEntry(key, klen, val, vlen);
                goto restart;
            }
            if (isspace(*str)) {
                break;
            }
            str++;
            len--;
        }
        UINTN klen = str - key;
        AddEntry(key, klen, NULL, 0);
    }
}

const char* CmdlineGet(const char* key, const char* _default) {
    UINTN klen = strlen(key);
    for (UINTN n = 0; n < entry_count; n++) {
        if ((entry[n].klen == klen) && !CompareMem(key, entry[n].key, klen)) {
            return entry[n].val;
        }
    }
    return _default;
}

UINT32 GetIntCmdline(const char* key, UINT32 _default) {
    const char* val = CmdlineGet(key, NULL);
    if (val == NULL) {
        return _default;
    }
    return atol(val);
}

void InitCmdline(IN EFI_HANDLE ImgHandle, IN EFI_SYSTEM_TABLE* SysTable) {
  UINT64 CmdSize;
  RawCmd = (char*)LoadBootModule(ImgHandle, SysTable, L"cmdline.txt", &CmdSize);

  RawCmd[CmdSize] = '\0';

  Print(L"Cmdline: %a\n\r", RawCmd);

  AppendCmdline((char*)RawCmd, CmdSize);
}