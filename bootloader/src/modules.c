#include <modules.h>

#include <Library/FileHandleLib.h>
#include <Protocol/LoadedImage.h>
#include <Library/MemoryAllocationLib.h>
#include <IndustryStandard/PeImage.h>
#include <Library/BaseLib.h>
#include <Library/UefiBootServicesTableLib.h>
#include <Guid/Acpi.h>

static EFI_FILE* LoadFile(EFI_FILE* Directory, CHAR16* Path, EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE* SystemTable) {
	EFI_FILE* LoadedFile;

	EFI_LOADED_IMAGE_PROTOCOL* LoadedImage;
	SystemTable->BootServices->HandleProtocol(ImageHandle, &gEfiLoadedImageProtocolGuid, (void**)&LoadedImage);

	EFI_SIMPLE_FILE_SYSTEM_PROTOCOL* FileSystem;
	SystemTable->BootServices->HandleProtocol(LoadedImage->DeviceHandle, &gEfiSimpleFileSystemProtocolGuid, (void**)&FileSystem);

	if (Directory == NULL){
		FileSystem->OpenVolume(FileSystem, &Directory);
	}

	EFI_STATUS s = Directory->Open(Directory, &LoadedFile, Path, EFI_FILE_MODE_READ, EFI_FILE_READ_ONLY);
	if (s != EFI_SUCCESS){
		return NULL;
	}
	return LoadedFile;

}

VOID* LoadBootModule(
  IN EFI_HANDLE ImgHandle,
  IN EFI_SYSTEM_TABLE* SysTable,
  IN CHAR16* Path, 
  OUT UINT64* Size
) {
    EFI_FILE* LoadingFile = LoadFile(NULL, Path, ImgHandle, SysTable);
    UINT64 FSize;
    FileHandleGetSize(LoadingFile, &FSize);

    VOID* RawFile = AllocatePool(FSize);
    LoadingFile->Read(LoadingFile, &FSize, RawFile);

    *Size = FSize;
    return RawFile;
}

// PE32+ Loader (aka Kernel Loader) =======================================================================

#define PAGE_SIZE (4096)
#define PAGE_MASK (PAGE_SIZE - 1)

#define EFI_BYTES_TO_PAGES(n) (((n) + PAGE_MASK) / PAGE_SIZE)

static char* getNtHdrs(char *pe_buffer) {
	if (pe_buffer == NULL) return NULL;

	EFI_IMAGE_DOS_HEADER *idh = (EFI_IMAGE_DOS_HEADER*)pe_buffer;
	if (idh->e_magic != EFI_IMAGE_DOS_SIGNATURE) {
		return NULL;
	}
	const UINT64 kMaxOffset = 1024;
	UINT64 pe_offset = idh->e_lfanew;
	if (pe_offset > kMaxOffset) return NULL;
	EFI_IMAGE_NT_HEADERS64 *inh = (EFI_IMAGE_NT_HEADERS64 *)((char*)pe_buffer + pe_offset);
	if (inh->Signature != EFI_IMAGE_NT_SIGNATURE) return NULL;
	return (char*)inh;
}

static EFI_IMAGE_DATA_DIRECTORY* getPeDir(VOID* pe_buffer, size_t dir_id) {
	if (dir_id >= 16) return NULL;

	char* nt_headers = getNtHdrs((char*)pe_buffer);
	if (nt_headers == NULL) return NULL;

	EFI_IMAGE_DATA_DIRECTORY* peDir = NULL;

	EFI_IMAGE_NT_HEADERS64* nt_header = (EFI_IMAGE_NT_HEADERS64*)nt_headers;
	peDir = &(nt_header->OptionalHeader.DataDirectory[dir_id]);

	if (peDir->VirtualAddress == NULL) {
		return NULL;
	}
	return peDir;
}

VOID* LoadPE32Plus(IN EFI_HANDLE ImgHandle, IN EFI_SYSTEM_TABLE* SysTable) {
	UINT64 fileSize = -1;
	char *data = LoadBootModule(ImgHandle, SysTable, L"cxkrnl.exe", &fileSize);
	char* pImageBase = NULL;
	VOID* preferAddr = 0;
	EFI_IMAGE_NT_HEADERS64 *ntHeader = (EFI_IMAGE_NT_HEADERS64 *)getNtHdrs(data);
	if (!ntHeader)  {
		Print(L"== [ERROR] File %s isn't a PE file.\n\r", L"cxkrnl.exe");
		return NULL;
	}

	EFI_IMAGE_DATA_DIRECTORY* relocDir = getPeDir(data, EFI_IMAGE_DIRECTORY_ENTRY_BASERELOC);
	preferAddr = (VOID*)ntHeader->OptionalHeader.ImageBase;
	Print(L"== [INFO] Prefered Image base is at 0x%x\n\r", preferAddr);

        (char *)gBS->AllocatePages(AllocateAddress, EfiLoaderData, EFI_BYTES_TO_PAGES(ntHeader->OptionalHeader.SizeOfImage), &preferAddr);
        pImageBase = (char*)preferAddr;
        if (!pImageBase && !relocDir) {
		Print(L"== [WARNING] Unable to Allocate Image Base At 0x%x\n\r", preferAddr);
		return NULL;
	}
	if (!pImageBase && relocDir) {
		(char *)gBS->AllocatePool(EfiLoaderData, ntHeader->OptionalHeader.SizeOfImage, &pImageBase);
		if (!pImageBase) {
			Print(L"== [ERROR] Memory Allocation Failed!\n\r");
			return NULL;
		}
	}
	
	ntHeader->OptionalHeader.ImageBase = (UINT64)pImageBase;
	CopyMem(pImageBase, data, ntHeader->OptionalHeader.SizeOfHeaders);

	EFI_IMAGE_SECTION_HEADER * SectionHeaderArr = (EFI_IMAGE_SECTION_HEADER *)((UINT64)ntHeader + sizeof(EFI_IMAGE_NT_HEADERS64));
	for (int i = 0; i < ntHeader->FileHeader.NumberOfSections; i++) {
		CopyMem
		(
			(VOID*)((UINT64)pImageBase + SectionHeaderArr[i].VirtualAddress),
			(VOID*)((UINT64)data + SectionHeaderArr[i].PointerToRawData),
			SectionHeaderArr[i].SizeOfRawData
		);
	}
	UINT64 retAddr = (UINT64)(pImageBase) + ntHeader->OptionalHeader.AddressOfEntryPoint;
	Print(L"== [INFO] EntryPoint Located at 0x%x\n\r", retAddr);
        return retAddr;
}

LOADER_BLOCK* Retblock = NULL;

EFI_STATUS LoadKernel(IN EFI_HANDLE ImgHandle, IN EFI_SYSTEM_TABLE* SysTable) {
    EFI_STATUS Status = EFI_SUCCESS;

    // fully-load the kernel
    VOID* Entry = LoadPE32Plus(ImgHandle, SysTable);

    // setup the struct
    Retblock = (LOADER_BLOCK*)AllocateReservedPool(sizeof(LOADER_BLOCK));
    SetMem(Retblock, sizeof(LOADER_BLOCK), 0);

    // cmdline
    Retblock->Cmdline = GetRawCmd();
	
	// Acpi information
	void* acpi_table = NULL;
    if (!EFI_ERROR(EfiGetSystemConfigurationTable(&gEfiAcpi20TableGuid, &acpi_table))) {
        Retblock->Rsdp = (UINT64)AllocateReservedCopyPool(36, acpi_table);
		Print(L"== [INFO] Acpi 2.0 Located!\n\r");
    } else if (!EFI_ERROR(EfiGetSystemConfigurationTable(&gEfiAcpi10TableGuid, &acpi_table))) {
        Retblock->Rsdp = (UINT64)AllocateReservedCopyPool(20, acpi_table);
		Print(L"== [INFO] Acpi 1.0 Located!\n\r");
    } else {
        Print(L"== [WARN] No ACPI table found, RSDP set to NULL\n\r");
    }

    // graphics info
    Retblock->Fbuf = *GetGOPInfo();

    Print(L"== [INFO] Setup Complete!\n\r");


    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // No prints from here
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    // setup the mmap information
    UINT8 TmpMemoryMap[1];
    UINTN MemoryMapSize = sizeof(TmpMemoryMap);
    UINTN MapKey = 0;
    UINTN DescriptorSize = 0;
    UINT32 DescriptorVersion = 0;
    gBS->GetMemoryMap(&MemoryMapSize, (EFI_MEMORY_DESCRIPTOR *) TmpMemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion);

    // allocate space for the efi mmap and take into
    // account that there will be changes
    MemoryMapSize += EFI_PAGE_SIZE;
    EFI_MEMORY_DESCRIPTOR* MemoryMap = AllocatePool(MemoryMapSize);

    // call it
    gBS->GetMemoryMap(&MemoryMapSize, MemoryMap, &MapKey, &DescriptorSize, &DescriptorVersion);
    UINTN EntryCount = (MemoryMapSize / DescriptorSize);

    // Exit the memory services
    gBS->ExitBootServices(gImageHandle, MapKey);

    Retblock->Mmap = MemoryMap;
	Retblock->MmapCount = EntryCount;
	Retblock->DescSize = DescriptorSize;
 
    void (*KernelStart)(LOADER_BLOCK*) = ((void (*)(LOADER_BLOCK*) ) Entry);
    KernelStart(Retblock);

cleanup:
    return Status;
}
