[BITS 64]

global JumpToKernel
extern Retblock
JumpToKernel:
    mov rbx, rcx
    mov rcx, Retblock
    jmp rbx
