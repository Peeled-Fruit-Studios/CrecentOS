#pragma once

#include <Uefi.h>

void AppendCmdline(const char* str, UINTN len);

void SetCmdline(const char* key, const char* val);

const char* CmdlineGet(const char* key, const char* _default);
UINT32 GetIntCmdline(const char* key, UINT32 _default);

// obtain the entire commandline as a string
UINTN CmdlineToString(char* ptr, UINTN max);

void InitCmdline(
  IN EFI_HANDLE ImgHandle,
  IN EFI_SYSTEM_TABLE* SysTable
);
