#pragma once

#include <Uefi.h>

typedef struct __fbuf {
  void* Base;
  UINT32 BSize;
  UINT32 Width;
  UINT32 Height;
  UINT32 Pitch;
} FrameBuffer;

FrameBuffer* GetGOPInfo();
