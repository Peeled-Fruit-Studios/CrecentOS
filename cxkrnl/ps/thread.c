#include <ps.h>
#include <kd.h>

// largest tid
static UINT16 curr_tid = 0;

KTHREAD* 
PsCreateThread(void (*entry)(UINT16), UINT8 Priority, UINT8 mode, void* rsp, UINT64 pagemap) {
    // 65535 = UINT16_MAX because a tid is 16-bits
    if (curr_tid == 65535) {
        LOG(LOG_FATAL, "System has reached the max number of threads and is unable to create more. : - (\n");
        return NULL;
    }

    // allocate memory for the new task and its stack
    KTHREAD* ntask = MmAllocatePool(sizeof(KTHREAD), TAG_PS);
    ntask->kstack_limit = MmAllocatePool(KSTACK_SIZE, TAG_PS);
    ntask->kstack_top = (UINT64)ntask->kstack_limit + KSTACK_SIZE;

    // create the stack frame and update the state to defaults
    TaskState* ntask_state = (void*)((UINT64)ntask->kstack_top - sizeof(TaskState));
    if (mode == THREAD_KERNEL_MODE) {
        ntask_state->cs = KMODE_CS;
        ntask_state->ss = KMODE_SS;
    } else {
        ntask_state->cs = UMODE_CS;
        ntask_state->ss = UMODE_SS;
    }
    ntask_state->rflags = RFLAGS_DEFAULT;
    ntask_state->rip = (UINT64)entry;
    ntask_state->rsp = rsp ? (UINT64)rsp : (UINT64)ntask->kstack_top;
    ntask_state->rdi = curr_tid; // pass the tid to the task

    // initialize the task
    if (pagemap)
        ntask->cr3 = pagemap;
    else
        ntask->cr3 = __readcr3();

    ntask->kstack_top = ntask_state;
    ntask->tid = curr_tid;
    ntask->Priority = Priority;
    ntask->last_tick = 0;
    ntask->Status = THREAD_READY;
    ntask->wakeuptime = 0;

    curr_tid++;
    return ntask;
}

UINT16
PsAddThread(void (*start)(UINT16), UINT8 priority, UINT8 mode, void* rsp, UINT64 pagemap) {
    KTHREAD* t = PsCreateThread(start, priority, mode, rsp, pagemap);
    if (t) {
        PspInsertThread(t);
        return t->tid;
    }
    return -1;
}
