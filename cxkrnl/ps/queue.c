#include <ps.h>

KTHREAD*
PsQueuePop(THREAD_QUEUE* q);

VOID
PsQueuePush(THREAD_QUEUE* q, KTHREAD* t) {
    t->prev = NULL;
    t->next = q->front;
    if (q->front)
        q->front->prev = t;
    else
        q->back = t;
    q->front = t;
}

VOID 
PsInsertAfter(THREAD_QUEUE* q, KTHREAD* a, KTHREAD* t) {
    t->prev = NULL;
    t->next = NULL;
    if (!a) {
        PsQueuePush(q, t);
    } else if (a == q->back) {
        a->next = t;
        t->prev = a;
        q->back = t;
    } else {
        KTHREAD* b = a->next;
        b->prev = t;
        a->next = t;
        t->prev = a;
        t->next = b;
    }
}

KTHREAD*
PsQueuePop(THREAD_QUEUE* q) {
    if (!q->back)
        return NULL;
    KTHREAD* ret = q->back;
    q->back = ret->prev;
    if (!q->back)
        q->front = NULL;
    else
        q->back->next = NULL;
    return ret;
}

KTHREAD*
PsQueueFind(THREAD_QUEUE* q, UINT16 tid) {
    for (KTHREAD* t = q->front; t; t = t->next)
        if (t->tid == tid)
            return t;
    return NULL;
}

