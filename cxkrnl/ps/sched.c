#include <ps.h>

#define TIMESLICE_DEFAULT RTL_MIL_TO_NANOS(1)
#define CPU_MAX 8

static KSPIN_LOCK SchedLock;

// an idle task for each cpu
static KTHREAD* tasks_idle[CPU_MAX];

// currently running task
static KTHREAD* tasks_running[CPU_MAX];

// tasks grouped by priority
static THREAD_QUEUE tasks_bg;
static THREAD_QUEUE tasks_min;
static THREAD_QUEUE tasks_mid;
static THREAD_QUEUE tasks_max;

// sleeping tasks arranged in descending order of their wakeup time
static THREAD_QUEUE tasks_asleep;

// temporary space to hold dead tasks, before janitor cleans them
static THREAD_QUEUE tasks_dead;

extern void PspStartCswitch(void* v);
extern void PspFinishCswitch(KTHREAD* next);

__declspec(noreturn) static void IdleThread(UINT16 tid) {
    (void)tid;
    while (TRUE)
        __hlt();
}

// the janitor, runs every second to clean up dead tasks
// TODO: run it on demand
__declspec(noreturn) static void PsJanitorThread(UINT16 tid) {
    (void)tid;
    while (TRUE) {
        KeAcquireSpinlock(&SchedLock);
        KTHREAD* t;
        while ((t = PsQueuePop(&tasks_dead))) {
            MmReleasePool(t->kstack_limit);
            MmReleasePool(t);
        }
        KeReleaseSpinlock(&SchedLock);
        PspThreadSleep(RTL_SECONDS_TO_NANOS(1));
    }
}

// adds to the sleeping tasks list
static void PspAddSleeping(KTHREAD* t) {
    if (!tasks_asleep.front)
        PsQueuePush(&tasks_asleep, t);
    else if (tasks_asleep.back->wakeuptime > t->wakeuptime)
        PsInsertAfter(&tasks_asleep, tasks_asleep.back, t);
    else {
        for (KTHREAD* i = tasks_asleep.front; i; i = i->next) {
            if (i->wakeuptime <= t->wakeuptime) {
                PsInsertAfter(&tasks_asleep, i->prev, t);
                break;
            }
        }
    }
}

static void PspAddThread(KTHREAD* t) {
    if (t->Status == THREAD_SLEEPING) {
        PspAddSleeping(t);
        return;
    }

    switch (t->Priority) {
    case PRIORITY_BG:
        PsQueuePush(&tasks_bg, t);
        return;
    case PRIORITY_MIN:
        PsQueuePush(&tasks_min, t);
        return;
    case PRIORITY_MID:
        PsQueuePush(&tasks_mid, t);
        return;
    case PRIORITY_MAX:
        PsQueuePush(&tasks_max, t);
        return;
    }
}

void PspInsertThread(KTHREAD* t) {
    KeAcquireSpinlock(&SchedLock);
    PspAddThread(t);
    KeReleaseSpinlock(&SchedLock);
}

void PspSchedKill() {
    KeAcquireSpinlock(&SchedLock);
    KTHREAD* curr = tasks_running[KeQueryCurrentProcessor()->CpuNum];
    curr->Status = THREAD_DEAD;
    PsQueuePush(&tasks_dead, curr);
    KeReleaseSpinlock(&SchedLock);

    // wait for scheduler
    __halt();
}
