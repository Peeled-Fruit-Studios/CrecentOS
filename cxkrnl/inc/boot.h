#pragma once

#include <cxkrnl.h>

typedef struct __fbuf {
  void* Base;
  UINT32 BSize;
  UINT32 Width;
  UINT32 Height;
  UINT32 BPP;
} FrameBuffer;

typedef struct _EFI_MMAP {
	UINT32 Type;
	UINT64 Physical; // NOTE: Always Use Physical
	UINT64 Virtual;
	UINT64 Pages;
	UINT64 Attrib;
} EFI_MMAP;

typedef struct __lblock {
  FrameBuffer Fbuf;
  UINT64 Magic;
  char* Cmdline;
  UINT32 MmapCount;
  UINT32 DescSize;
  EFI_MMAP* Mmap;
  void* Rsdp;
} LOADER_BLOCK;

VOID KeMain(IN LOADER_BLOCK* LBlock);
