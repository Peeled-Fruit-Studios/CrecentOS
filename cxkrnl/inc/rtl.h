#pragma once

#include <cxkrnl.h>
#include <stdarg.h>

// Bitmap Functions

typedef struct _bmap {
	UINT64 Size;
	UINT8* Buffer;
} BITMAP;

VOID RtlBitmapSet(IN BITMAP* Map, IN UINT64 Index, IN BOOLEAN Value);
BOOLEAN RtlBitmapCheck(IN BITMAP* Map, IN UINT64 Index);

// String Functions

UINT64 
RtlVaStrPrint(char *print_buf, UINT64 limit, const char *fmt, va_list args);

UINT64 
RtlStrPrint(char *buf, UINT64 limit, const char *fmt, ...);


UINT32 __strlen(char* str);

int 
RtlCompareString(const char *s1, const char *s2, UINT32 n);

// Memory Functions

VOID RtlClearMem(IN VOID* Mem, UINT64 Count);

VOID RtlSetMem(void* ptr, int Val, UINT64 Count);

#define RTL_NUM_TO_PAGES(num) (((num) + 4096 - 1) / 4096)
