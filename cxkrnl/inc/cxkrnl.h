#pragma once

// Declare Types First

#define VOID void
#define NULL ((void*)0)

#define IN
#define OUT
#define IN_OUT

#define BOOLEAN char
#define TRUE 1
#define FALSE 0

typedef unsigned long long UINT64;
typedef unsigned int UINT32;
typedef unsigned short UINT16;
typedef unsigned char UINT8;

typedef UINT32 UINT;

typedef long long INT64;

typedef volatile long long KSPIN_LOCK;

__forceinline
void
KeAcquireSpinlock(IN KSPIN_LOCK* Lock) {
	while ( _InterlockedCompareExchange64( ( volatile long long* )Lock, 1, 0 ) != 0 );
}

__forceinline
void
KeReleaseSpinlock(IN KSPIN_LOCK* Lock) {
	*Lock = 0;
}

typedef struct {
	UINT32 CpuNum;
} KPCB;

KPCB*
KeQueryCurrentProcessor();
