#pragma once

#include <cxkrnl.h>

typedef enum {
	OB_SYMLINK,
	OB_DIR,
	OB_DUMMY
} ObjectType;

typedef struct _ObjectHeader {
	ObjectType Type;
	UINT64 Size;
	BOOLEAN Permanant;
	UINT64 RefCount;
} ObjectHeader;

typedef struct _OBJECT_SYMBOLIC_LINK {
	ObjectHeader Header;
	char* LinkName;
	char* LinkTargetName;
	ObjectHeader** LinkTarget;
} OBJECT_SYMBOLIC_LINK;

typedef struct _OBJECT_DIRECTORY {
	ObjectHeader Header;
	char name[128];
	int DirCount;
	int DirMax;
	ObjectHeader** Children;
} OBJECT_DIRECTORY;

// A dummy object that just contains a message
typedef struct _OBJECT_DUMMY {
	ObjectHeader Header;
	char* Message;
} OBJECT_DUMMY;

#define TAG_OB							    '  bO'

#define OB_HEADER( object )				    (object->Header)
#define HDR2OBJ( object, type )				((type*)(object))

#define SYMLINK_GET_OBJ( object, type ) (type*)(object->LinkTarget)

VOID
ObInitializeObjectManager();

VOID
ObCreateDirectory(IN char* name, IN_OUT OBJECT_DIRECTORY* Dir, IN int Dcount, IN ObjectHeader* Children);

VOID
ObCreateSymLink(IN char* name, IN_OUT OBJECT_SYMBOLIC_LINK* SymLink, IN char* Target, IN ObjectHeader* TargetHeader);

VOID
ObAppendDirectory(IN_OUT OBJECT_DIRECTORY* Dir, IN ObjectHeader* Child);

ObjectHeader*
ObGetChildByIndex(IN OBJECT_DIRECTORY* Obj, IN int index);
