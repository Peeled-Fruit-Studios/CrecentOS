#pragma once

#include <cxkrnl.h>

#define TAG_PS '  sP'

#define KMODE_CS 0x08
#define KMODE_SS 0x10
#define UMODE_CS 0x1b
#define UMODE_SS 0x23
#define RFLAGS_DEFAULT 0x0202

#define KSTACK_SIZE 0x1000

#define PRIORITY_IDLE 0
#define PRIORITY_BG 5
#define PRIORITY_MIN 10
#define PRIORITY_MID 35
#define PRIORITY_MAX 70

typedef enum {
    THREAD_KERNEL_MODE,
    THREAD_USER_MODE
} THREAD_MODE;

typedef enum {
    THREAD_READY,
    THREAD_RUNNING,
    THREAD_SLEEPING,
    THREAD_DEAD
} THREAD_STATUS;

typedef struct {
    UINT64 r15;
    UINT64 r14;
    UINT64 r13;
    UINT64 r12;
    UINT64 r11;
    UINT64 r10;
    UINT64 r9;
    UINT64 r8;
    UINT64 rbp;
    UINT64 rdi;
    UINT64 rsi;
    UINT64 rdx;
    UINT64 rcx;
    UINT64 rbx;
    UINT64 rax;
    UINT64 rip;
    UINT64 cs;
    UINT64 rflags;
    UINT64 rsp;
    UINT64 ss;
} TaskState;

typedef struct _KTHREAD {
	UINT16 tid;
	UINT8 Mode;
	UINT8 Status;
	UINT8 Priority;
	
    void* kstack_top;     // kernel stack top
    UINT64 cr3;            // virtual address space

    UINT64 last_tick;      // last tick at which task ran
    UINT64 wakeuptime;     // time at which task should wake up
    void* kstack_limit;    // kernel stack limit

    struct _KTHREAD* next;
    struct _KTHREAD* prev;
} KTHREAD;

typedef struct {
    KTHREAD* front;
    KTHREAD* back;
    UINT64 len;
} THREAD_QUEUE;

KTHREAD*
PsQueuePop(THREAD_QUEUE* q);

VOID
PsQueuePush(THREAD_QUEUE* q, KTHREAD* t);

KTHREAD*
PsQueueFind(THREAD_QUEUE* q, UINT16 tid);

VOID 
PsInsertAfter(THREAD_QUEUE* q, KTHREAD* a, KTHREAD* t);

KTHREAD* 
PsCreateThread(void (*entry)(UINt16), UINT8 Priority, UINT8 mode, void* rsp, UINT64 pagemap);

UINT16 
PsAddThread(void (*start)(UINT16), UINT8 priority, UINT8 mode, void* rsp, UINT64 pagemap); // Same as above, but adds thread to the scheduler
