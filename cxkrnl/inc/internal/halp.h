#pragma once

#include <cxkrnl.h>

#define IA32_MSR_APIC_BASE              0x0000001B
#define IA32_MSR_FS_BASE                0xC0000100
#define IA32_MSR_GS_BASE                0xC0000101
#define IA32_MSR_GS_KERNEL_BASE         0xC0000102
#define IA32_MSR_EFER                   0xC0000080
#define IA32_MSR_STAR                   0xC0000081
#define IA32_MSR_LSTAR                  0xC0000082
#define IA32_MSR_SFMASK                 0xC0000084

#define CR4_PCIDE       ( 1 << 17 )
#define CR4_PGE         ( 1 << 7 )
#define CR4_PAE         ( 1 << 5 )
#define CR4_PKE         ( 1 << 22 )
#define CR4_LA57        ( 1 << 12 )
#define CR4_UMIP        ( 1 << 11 )
#define CR4_SMEP        ( 1 << 20 )
#define CR4_SMAP        ( 1 << 21 )

#define EFER_NXE        ( 1 << 11 )
#define EFER_SCE        ( 1 << 0 )

#define CPUID_EAX 0
#define CPUID_EBX 1
#define CPUID_ECX 2
#define CPUID_EDX 3

__forceinline
VOID
KiSafeCpuid(
    IN int IdRegisters[ 4 ],
    IN int Leaf,
    IN int SubLeaf
)
{
    if ( Leaf > 0x80000000 ) {

        KiCpuid( IdRegisters, 0x80000000, 0 );

        if ( Leaf > IdRegisters[ CPUID_EAX ] ) {

            IdRegisters[ 0 ] = 0;
            IdRegisters[ 1 ] = 0;
            IdRegisters[ 2 ] = 0;
            IdRegisters[ 3 ] = 0;
            return;
        }
    }

    KiCpuid( IdRegisters, Leaf, SubLeaf );
}
