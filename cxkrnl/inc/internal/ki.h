#pragma once

#define KiMsrRead           __readmsr 
#define KiMsrWrite          __writemsr
#define KiMsrRead           __readmsr 
#define KiMsrWrite          __writemsr
#define KiLoadInterrupt     __lidt
#define KiLoadGlobal        _lgdt
#define KiLoadTask          __ltr
#define KiProcessorHalt     __halt
#define KiCpuid             __cpuidex
#define KiInvpcid           _invpcid
#define KiInvlpg            __invlpg
#define KiCr0Read           __readcr0
#define KiCr0Write          __writecr0
#define KiCr4Read           __readcr4
#define KiCr4Write          __writecr4

typedef enum _PROCESSOR_FEATURE {
    CPU_NX_ENABLED = 0,
    CPU_PCID_ENABLED,
    CPU_XSAVE_ENABLED,
    CPU_PAGE1GB_ENABLED,
    CPU_PKU_ENABLED,
    CPU_PGE_ENABLED,
    CPU_SMEP_ENABLED,
    CPU_SMAP_ENABLED,
    CPU_PAT_ENABLED,
    CPU_FXSR_ENABLED,
    CPU_SC_ENABLED,
    CPU_SSE3_ENABLED,
    FEATURES_MAXIMUM
} PROCESSOR_FEATURE;

