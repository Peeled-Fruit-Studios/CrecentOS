#pragma once

#include <cxkrnl.h>

// Cache defs and decls ============================================================================

typedef union _IA32_PAT_MSR {
    struct {
        UINT64 Pa0 : 3;
        UINT64 Reserved0 : 5;
        UINT64 Pa1 : 3;
        UINT64 Reserved1 : 5;
        UINT64 Pa2 : 3;
        UINT64 Reserved2 : 5;
        UINT64 Pa3 : 3;
        UINT64 Reserved3 : 5;
        UINT64 Pa4 : 3;
        UINT64 Reserved4 : 5;
        UINT64 Pa5 : 3;
        UINT64 Reserved5 : 5;
        UINT64 Pa6 : 3;
        UINT64 Reserved6 : 5;
        UINT64 Pa7 : 3;
        UINT64 Reserved7 : 5;
    };

    UINT64    Long;
} IA32_PAT_MSR, *PIA32_PAT_MSR;


#define IA32_MSR_PAT    0x00000277

#define MEM_TYPE_UC     0x00
#define MEM_TYPE_WC     0x01
#define MEM_TYPE_WT     0x04
#define MEM_TYPE_WP     0x05
#define MEM_TYPE_WB     0x06
#define MEM_TYPE_UC1    0x07


// pfdb (Page Frame Database) defs and decls ========================================================

VOID
MiLockPage(IN VOID* address);

VOID
MiLockPages(IN VOID* address, IN UINT64 pageCount);

VOID
MiReleasePage(IN VOID* address);

VOID
MiReleasePages(IN VOID* address, IN UINT64 pageCount);

VOID 
MiReservePage(IN VOID* address);

VOID
MiReservePages(IN VOID* address, IN UINT64 pageCount);

// Pool defs and decls ==============================================================================

typedef struct _POOL_HEADER {
	UINT64 len;
	UINT64 Tag;
	struct _POOL_HEADER* next;
	struct _POOL_HEADER* last;
	BOOLEAN free;
} POOL_HEADER;

VOID
MiCombineForward(POOL_HEADER* Hdr);

VOID
MiCombineBackward(POOL_HEADER* Hdr);

VOID
MiExpandHeap(UINT64 length);

POOL_HEADER*
MiSplitPool(POOL_HEADER* Hdr, UINT64 len);
