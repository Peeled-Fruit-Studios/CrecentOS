#pragma once

#include <cxkrnl.h>
#include <kd.h>

#include <stdarg.h>

typedef enum {
	// Informs the developer about something (total memory, process count, etc.)
	LOG_INFO,
	
	// Placed by the developer as a temporary source of information (not logged on release builds)
	LOG_DEBUG,
	
	// Warns the developer about a system problem/issue
	LOG_WARNING,
	
	// Printed before a bugcheck or a fatal error to give more information (Always Fatal)
	LOG_FATAL
} LOG_MODE;

#define LOG(Mode, Str, ...)  DbgLogStr(Mode, Str, ##__VA_ARGS__)

#define DEBUG(Str, ...)  DbgLogStr(LOG_DEBUG, Str, ##__VA_ARGS__)
#define INFO(Str, ...)  DbgLogStr(LOG_INFO, Str, ##__VA_ARGS__)

VOID
DbgLogStr(LOG_MODE mode, char* str, ...);

#define COM1 0x3f8
#define COM2 0x2F8

// NOTE: COM2 is used since edk2 uses COM1 in qemu and it messes up output

VOID KdInitCom();
VOID KdcomWriteByte(UINT8 a);
