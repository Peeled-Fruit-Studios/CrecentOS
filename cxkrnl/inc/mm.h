#include <cxkrnl.h>

extern UINT64 freeMemory;
extern UINT64 reservedMemory;
extern UINT64 usedMemory;
extern BOOLEAN PfdbOn;

VOID*
MmRequestPage();

VOID
MmFreePage(IN VOID* address);

VOID
MmFreePages(IN VOID* address, IN UINT64 pageCount);

#define PAGE_PRESENT 1 << 0
#define PAGE_READWRITE 1 << 1
#define PAGE_USER 1 << 2
#define PAGE_WRITETHROUGH 1 << 3
#define PAGE_CACHE_DISABLE 1 << 4
#define PAGE_WRITECOMBINE 1 << 7

#define PAGE_FLAGS_DEFAULT (PAGE_PRESENT | PAGE_READWRITE)
#define PAGE_FLAGS_MMIO (PAGE_FLAGS_DEFAULT | PAGE_CACHE_DISABLE)
#define PAGE_FLAGS_USERMODE (PAGE_FLAGS_DEFAULT | PAGE_USER)

#define VIRT_TO_PHYS(a) ((UINT64)(a))
#define PHYS_TO_VIRT(a) ((UINT64)(a))

#define MmLoadAspace(k) __writecr3(k.PML4)

typedef struct {
    UINT64* PML4;
} ADDRESS_SPACE;

extern ADDRESS_SPACE kspace;

VOID 
MmMapMemory(ADDRESS_SPACE* aspace, UINT64 Virtual, UINT64 Physical, UINT64 Pages, UINT64 flags);
VOID 
MmUnmapMemory(ADDRESS_SPACE* aspace, UINT64 Virtual, UINT64 Pages);

VOID
MmInitHeap(void* HeapAddress, UINT64 PageCount);

VOID* 
MmAllocatePool(UINT64 size, UINT64 Tag);

VOID*
MmRelocatePool(UINT64 ptr, UINT64 size);

VOID
MmReleasePool(void* Address);
