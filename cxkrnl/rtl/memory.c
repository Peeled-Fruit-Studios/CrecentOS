#include <rtl.h>

static void memset(void* s, int c, UINT64 n) {
	volatile char* ch = (char*)s;
	for(volatile UINT64 k = 0; k < n; k++)
		ch[k] = c;
}

VOID RtlSetMem(void* ptr, int Val, UINT64 Count) {
	memset(ptr, Val, Count);
}

VOID RtlClearMem(IN VOID* Mem, UINT64 Count) {
	memset(Mem, 0, Count);
}

VOID RtlCopyMem(IN VOID* Dest, IN VOID* Src, IN UINT64 Count) {
	for(volatile UINT64 k = 0; k < Count; k++)
		((char*)Dest)[k] = ((char*)Src)[k];
}

