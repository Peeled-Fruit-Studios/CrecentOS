#include <rtl.h>

static const char *base_digits = "0123456789abcdef";

static void prn_str(char *print_buf, UINT64 limit, UINT64 *print_buf_i, const char *string) {
    UINT64 i;

    for (i = 0; string[i]; i++) {
        if (*print_buf_i == (limit - 1))
            break;
        print_buf[(*print_buf_i)++] = string[i];
    }

    print_buf[*print_buf_i] = 0;
}

static void prn_nstr(char *print_buf, UINT64 limit, UINT64 *print_buf_i, const char *string, UINT64 len) {
    UINT64 i;

    for (i = 0; i < len; i++) {
        if (*print_buf_i == (limit - 1))
            break;
        print_buf[(*print_buf_i)++] = string[i];
    }

    print_buf[*print_buf_i] = 0;
}

static void prn_char(char *print_buf, UINT64 limit, UINT64 *print_buf_i, char c) {
    if (*print_buf_i < (limit - 1)) {
        print_buf[(*print_buf_i)++] = c;
    }

    print_buf[*print_buf_i] = 0;
}


static void prn_i(char *print_buf, UINT64 limit, UINT64 *print_buf_i, INT64 x) {
    int i;
    char buf[20] = {0};

    if (!x) {
        prn_char(print_buf, limit, print_buf_i, '0');
        return;
    }

    int sign = x < 0;
    if (sign) x = -x;

    for (i = 18; x; i--) {
        buf[i] = (x % 10) + 0x30;
        x = x / 10;
    }
    if (sign)
        buf[i] = '-';
    else
        i++;

    prn_str(print_buf, limit, print_buf_i, buf + i);
}


static void prn_ui(char *print_buf, UINT64 limit, UINT64 *print_buf_i, UINT64 x) {
    int i;
    char buf[21] = {0};

    if (!x) {
        prn_char(print_buf, limit, print_buf_i, '0');
        return;
    }

    for (i = 19; x; i--) {
        buf[i] = (x % 10) + 0x30;
        x = x / 10;
    }

    i++;
    prn_str(print_buf, limit, print_buf_i, buf + i);
}

static void prn_x(char *print_buf, UINT64 limit, UINT64 *print_buf_i, UINT64 x) {
    int i;
    char buf[17] = {0};

    if (!x) {
        prn_str(print_buf, limit, print_buf_i, "0x0");
        return;
    }

    for (i = 15; x; i--) {
        buf[i] = base_digits[(x % 16)];
        x = x / 16;
    }

    i++;
    prn_str(print_buf, limit, print_buf_i, "0x");
    prn_str(print_buf, limit, print_buf_i, buf + i);
}

UINT64 RtlVaStrPrint(char *print_buf, UINT64 limit, const char *fmt, va_list args) {
    UINT64 print_buf_i = 0;

    for (;;) {
        while (*fmt && *fmt != '%')
            prn_char(print_buf, limit, &print_buf_i, *fmt++);

        if (!*fmt++)
            goto out;

        switch (*fmt++) {
            case 's': {
                char *str = (char *)va_arg(args, const char *);
                if (!str)
                    prn_str(print_buf, limit, &print_buf_i, "(null)");
                else
                    prn_str(print_buf, limit, &print_buf_i, str);
                break;
            }
            case 'S': {
                char *str = (char *)va_arg(args, const char *);
                UINT64 str_len = va_arg(args, UINT64);
                if (!str)
                    prn_str(print_buf, limit, &print_buf_i, "(null)");
                else
                    prn_nstr(print_buf, limit, &print_buf_i, str, str_len);
                break;
            }
            case 'd':
                prn_i(print_buf, limit, &print_buf_i, (INT64)va_arg(args, int));
                break;
            case 'u':
                prn_ui(print_buf, limit, &print_buf_i, (UINT64)va_arg(args, UINT32));
                break;
            case 'x':
                prn_x(print_buf, limit, &print_buf_i, (UINT64)va_arg(args, UINT32));
                break;
            case 'D':
                prn_i(print_buf, limit, &print_buf_i, va_arg(args, INT64));
                break;
            case 'U':
                prn_ui(print_buf, limit, &print_buf_i, va_arg(args, UINT64));
                break;
            case 'X':
                prn_x(print_buf, limit, &print_buf_i, va_arg(args, UINT64));
                break;
            case 'c': {
                char c = (char)va_arg(args, int);
                prn_char(print_buf, limit, &print_buf_i, c);
                break;
            }
            default:
                prn_char(print_buf, limit, &print_buf_i, '?');
                break;
        }
    }

out:
    return print_buf_i;
}

UINT64 RtlStrPrint(char *buf, UINT64 limit, const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);

    UINT64 ret = RtlVaStrPrint(buf, limit, fmt, args);

    va_end(args);

    return ret;
}


UINT32 __strlen(char* str) {
    UINT32 l = 0;
    while(*str) {
        str++;
        l++;
    }
    return l;
}

int RtlCompareString(const char *s1, const char *s2, UINT32 n) {
    for (UINT32 i = 0; i < n; i++) {
        char c1 = s1[i], c2 = s2[i];
        if (c1 != c2)
            return c1 - c2;
        if (!c1)
            return 0;
    }

    return 0;
}
