#include <rtl.h>

VOID
RtlBitmapSet(IN BITMAP* Map, IN UINT64 Index, IN BOOLEAN Value) {
	if (Index > Map->Size * 8) return;
    UINT64 byteIndex = Index / 8;
    UINT8 bitIndex = Index % 8;
    UINT8 bitIndexer = 0b10000000 >> bitIndex;
    Map->Buffer[byteIndex] &= ~bitIndexer;
    if (Value){
        Map->Buffer[byteIndex] |= bitIndexer;
    }
}

BOOLEAN 
RtlBitmapCheck(IN BITMAP* Map, IN UINT64 Index) {
	if (Index > Map->Size * 8) return FALSE;
    UINT64 byteIndex = Index / 8;
    UINT8 bitIndex = Index % 8;
    UINT8 bitIndexer = 0b10000000 >> bitIndex;
    if ((Map->Buffer[byteIndex] & bitIndexer) > 0){
        return TRUE;
    }
    return FALSE;
}

