#include <cxkrnl.h>
#include <internal/ki.h>
#include <internal/halp.h>

KPCB*
KeQueryCurrentProcessor() {
    // MSR_KERNEL_GS_BASE = 0xC0000102

    return ( KPCB* )__readmsr( 0xC0000102 );
}

BOOLEAN
KeProcessorFeatureEnabled (
    IN VOID*              Processor,
    IN PROCESSOR_FEATURE  Feature
)
{

    int CpuidRegisters[ 4 ];
    int CpuidRegisters2[ 4 ];

    if ( Feature > FEATURES_MAXIMUM ) {

        return FALSE;
    }

    if ( Processor == NULL ) {
        switch ( Feature ) {
        case CPU_NX_ENABLED:
            KiSafeCpuid( CpuidRegisters, 0x80000001, 0 );
            return ( CpuidRegisters[ CPUID_EDX ] >> 20 ) & 1;

        case CPU_PCID_ENABLED:
            KiSafeCpuid( CpuidRegisters, 1, 0 );
            KiSafeCpuid( CpuidRegisters2, 7, 0 );
            return ( ( CpuidRegisters[ CPUID_ECX ] >> 17 ) & 1 ) && ( ( CpuidRegisters2[ CPUID_EBX ] >> 10 ) & 1 );

        case CPU_SMEP_ENABLED:
            KiSafeCpuid( CpuidRegisters, 7, 0 );
            return ( CpuidRegisters[ CPUID_EBX ] >> 7 ) & 1;
 
       case CPU_SMAP_ENABLED:
            KiSafeCpuid( CpuidRegisters, 7, 0 );
            return ( CpuidRegisters[ CPUID_EBX ] >> 20 ) & 1;

        case CPU_PKU_ENABLED:
            KiSafeCpuid( CpuidRegisters, 7, 0 );
            return ( CpuidRegisters[ CPUID_ECX ] >> 3 ) & 1;

        case CPU_PAGE1GB_ENABLED:
            KiSafeCpuid( CpuidRegisters, 0x80000001, 0 );
            return ( CpuidRegisters[ CPUID_EDX ] >> 26 ) & 1;

        case CPU_PAT_ENABLED:
            KiSafeCpuid( CpuidRegisters, 1, 0 );
            return ( CpuidRegisters[ CPUID_EDX ] >> 16 ) & 1;

        case CPU_PGE_ENABLED:
            KiSafeCpuid( CpuidRegisters, 1, 0 );
            return ( CpuidRegisters[ CPUID_EDX ] >> 13 ) & 1;

        case CPU_FXSR_ENABLED:
            KiSafeCpuid( CpuidRegisters, 1, 0 );
            return ( CpuidRegisters[ CPUID_EDX ] >> 24 ) & 1;

        case CPU_SC_ENABLED:
            KiSafeCpuid( CpuidRegisters, 0x80000001, 0 );
            return ( CpuidRegisters[ CPUID_EDX ] >> 11 ) & 1;

        case CPU_SSE3_ENABLED:
            KiSafeCpuid( CpuidRegisters, 1, 0 );
            return CpuidRegisters[ CPUID_ECX ] & 1;

        default:
            __assume( 0 );
        }
    }
}
