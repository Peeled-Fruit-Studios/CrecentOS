#include <boot.h>
#include <kd.h>
#include <internal/ki.h>
#include <internal/halp.h>

KSPIN_LOCK InitLock;

static void KePrintSuccess(IN LOADER_BLOCK* LBlock) {
  unsigned char *fb_addr = (unsigned char *)LBlock->Fbuf.Base;

  for(UINT32 i = 0; i < LBlock->Fbuf.BSize; i++) {
    fb_addr[i] = 0;
  }

  // Let's try to paint a few pixels white in the top left, so we know
  // that we booted correctly.
  for (int i = 0; i < 128; i++) {
    for(int g = 0; g < 128; g++) {
      UINT32 fb_i = i + (LBlock->Fbuf.BPP / sizeof(UINT32)) * g;
      UINT32 *fb = (UINT32 *)fb_addr;
      fb[fb_i] = 0xff;
    }
  }
  
  INFO("init: Hello, World!\n");
}

// The entry point to the kernel
VOID KeMain(IN LOADER_BLOCK* LBlock) {
  // Get Spinlock
  KeAcquireSpinlock(&InitLock);
  
  // Clear the MSR GS Registers First.
  KiMsrWrite( IA32_MSR_GS_BASE, 0 );
  KiMsrWrite( IA32_MSR_GS_KERNEL_BASE, 0 );

  // Init logging
  KdInitSubsystem();

  // Setup Caching Properly.
  MmInitializeCaching();
  
  // Then Initialize the Memory Manager
  MmInitSystem(LBlock);
  
  // Next, Init the Object Manager
  ObInitializeObjectManager();

  // Load GDT
  HalpInitGDT();
  HalpInitIDT();

  // Then print success screen
  KePrintSuccess(LBlock);
  
  // Release Spinlock
  KeReleaseSpinlock(&InitLock);
  
  // Halt to avoid errors
  for(;;);
}
