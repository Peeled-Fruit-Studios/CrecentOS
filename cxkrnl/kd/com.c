#include <kd.h>

VOID KdInitCom() {
    __outbyte(COM1 + 2, 0);
    __outbyte(COM1 + 3, 0x80);
    __outbyte(COM1 + 0, 115200 / 9600);
    __outbyte(COM1 + 1, 0);
    __outbyte(COM1 + 3, 0x03);
    __outbyte(COM1 + 4, 0);
    __outbyte(COM1 + 1, 0x01);
 
   // If serial is not faulty set it in normal operation mode
   // (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
   // NOTE: When serial irq handler is implemented, uncomment.
   // outb(COM1 + 4, 0x0F);
}
static int KdcomTransmitEmpty() {
   return __inbyte(COM1 + 5) & 0x20;
}
 
VOID KdcomWriteByte(UINT8 a) {
   while (KdcomTransmitEmpty() == 0);
 
   __outbyte(COM1, a);
}
