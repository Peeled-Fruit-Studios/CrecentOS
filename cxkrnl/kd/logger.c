#include <kd.h>

static VOID DbgPrintLogStr(char* str) {
	for(UINT k = 0; k < __strlen(str); k++) {
		KdcomWriteByte(str[k]);
	}
	__outbytestring(0xE9, str, __strlen(str));
}

const char* DbgGetModeStr(LOG_MODE mode) {
	switch(mode) {
		case LOG_INFO:
		return "   [INFO] | ";
		break;
		case LOG_DEBUG:
		return "  [DEBUG] | ";
		break;
		case LOG_WARNING:
		return "[WARNING] | ";
		break;
		case LOG_FATAL:
		return "  [FATAL] | ";
		break;
	}
}

VOID DbgLogStr(LOG_MODE mode, char* str, ...) {
#ifdef _RELEASE	
    if(mode == LOG_DEBUG) return;
#endif
    va_list args;
    va_start(args, str);
	
	DbgPrintLogStr(DbgGetModeStr(mode));

    char buf[256];
    RtlVaStrPrint(buf, 256, str, args);

    DbgPrintLogStr(buf);
	va_end(args);
}

VOID KdInitSubsystem() {
	KdInitCom();
	DbgPrintLogStr("\n\nCrecentOS Kernel Mode Debugger\n");
	DbgPrintLogStr("Compiled on " __DATE__ " at " __TIME__"\n\n");
}
