#include <cxkrnl.h>
#include <boot.h>
#include <rtl.h>
#include <mm.h>
#include <kd.h>

#include <internal/ki.h>
#include <internal/mi.h>

extern BITMAP MainMap;

VOID MmInitSystem(IN LOADER_BLOCK *Block) {
	EFI_MMAP* Mmap = Block->Mmap;
	UINT64 EntryCount = Block->MmapCount;
	UINT64 DescSize = Block->DescSize;
	
	if (PfdbOn) return;

    PfdbOn = TRUE;

    VOID* largestFreeMemSeg = NULL;
    UINT64 largestFreeMemSegSize = 0;

    for (UINT64 i = 0; i < EntryCount; i++){
        EFI_MMAP* desc = (EFI_MMAP*)((UINT64)Mmap + (i * DescSize));
        if (desc->Type == 7){ // type = EfiConventionalMemory
            if (desc->Pages * 4096 > largestFreeMemSegSize)
            {
                largestFreeMemSeg = (void*) desc->Physical;
                largestFreeMemSegSize = desc->Pages * 4096;
            }
        }
    }

    UINT64 memorySize = 0;
	for(UINT64 f = 0; f < EntryCount; f++) {
		EFI_MMAP* desc = (EFI_MMAP*)((UINT64)Mmap + (f * DescSize));
		memorySize += desc->Pages * 4096;
	}
    freeMemory = memorySize;
    UINT64 bitmapSize = memorySize / 4096 / 8 + 1;

	MainMap.Size = bitmapSize;
    MainMap.Buffer = (UINT8*)largestFreeMemSeg;
    for (int i = 0; i < bitmapSize; i++){
        *(UINT8*)(MainMap.Buffer + i) = 0;
    }

	// Lock the bitmap (not sure if this is the right way)
    MiLockPages(MainMap.Buffer, MainMap.Size / 4096 + 1);
	
	// Lock the kernel (6MB of Memory)
	MiLockPages((void*)0x100000, (6 * 1024 * 1024) / 4096 + 1);
	
	// Lock the Framebuffer
	MiLockPages(Block->Fbuf.Base, RTL_NUM_TO_PAGES(Block->Fbuf.BSize));

    for (int i = 0; i < EntryCount; i++){
        EFI_MMAP* desc = (EFI_MMAP*)((UINT64)Mmap + (i * DescSize));
        if (desc->Type != 7){ // not efiConventionalMemory
            MiReservePages((void*)desc->Physical, desc->Pages);
        }
    }
    
	INFO("Memory Info:\n");
	INFO("   Total Memory - %u MB\n", (freeMemory + reservedMemory + usedMemory) / 1024 / 1024);
	INFO("   Used Memory  - %u MB\n", (reservedMemory + usedMemory) / 1024 / 1024);
	INFO("   Free Memory  - %u MB\n", (freeMemory) / 1024 / 1024);
    
	MmSyncMapping(Block);

	MmInitHeap((void*)0x0000100000000000, 0x10);
}

