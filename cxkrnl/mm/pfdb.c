#include <cxkrnl.h>
#include <rtl.h>

#include <internal/mi.h>
#include <kd.h>

BITMAP MainMap;
UINT64 freeMemory;
UINT64 reservedMemory;
UINT64 usedMemory;
UINT64 MainMapIndex = 0;
BOOLEAN PfdbOn = FALSE;

VOID*
MmRequestPage(){
    for (; MainMapIndex < MainMap.Size * 8; MainMapIndex++){
        if (RtlBitmapCheck(&MainMap, MainMapIndex) == TRUE) continue;
        MiLockPage((void*)(MainMapIndex * 4096));
        return (void*)(MainMapIndex * 4096);
    }

    return NULL;
}

VOID
MmFreePage(IN VOID* address){
    UINT64 index = (UINT64)address / 4096;
    if (RtlBitmapCheck(&MainMap, index) == FALSE) return;
    RtlBitmapSet(&MainMap, index, FALSE);
    freeMemory += 4096;
    usedMemory -= 4096;
    if (MainMapIndex > index) MainMapIndex = index;
}

VOID
MmFreePages(IN VOID* address, IN UINT64 pageCount){
    for (UINT64 t = 0; t < pageCount; t++){
        MmFreePage((void*)((UINT64)address + (t * 4096)));
    }
}

VOID
MiLockPage(IN VOID* address){
    UINT64 index = (UINT64)address / 4096;
    if (RtlBitmapCheck(&MainMap, index) == TRUE) return;
    RtlBitmapSet(&MainMap, index, TRUE);
    freeMemory -= 4096;
    usedMemory += 4096;
}

VOID
MiLockPages(IN VOID* address, IN UINT64 pageCount){
    for (UINT64 t = 0; t < pageCount; t++){
        MiLockPage((void*)((UINT64)address + (t * 4096)));
    }
}

VOID
MiReleasePage(IN VOID* address){
    UINT64 index = (UINT64)address / 4096;
    if (RtlBitmapCheck(&MainMap, index) == FALSE) return;
    RtlBitmapSet(&MainMap, index, FALSE);
    freeMemory += 4096;
    reservedMemory -= 4096;
    if (MainMapIndex > index) MainMapIndex = index;
}

VOID
MiReleasePages(IN VOID* address, IN UINT64 pageCount){
    for (UINT64 t = 0; t < pageCount; t++){
        MiReleasePage((void*)((UINT64)address + (t * 4096)));
    }
}

VOID MiReservePage(IN VOID* address){
    UINT64 index = (UINT64)address / 4096;
    if (RtlBitmapCheck(&MainMap, index) == TRUE) return;
    RtlBitmapSet(&MainMap, index, TRUE);
    freeMemory -= 4096;
    reservedMemory += 4096;
}

VOID
MiReservePages(IN VOID* address, IN UINT64 pageCount){
    for (UINT64 t = 0; t < pageCount; t++){
        MiReservePage((void*)((UINT64)address + (t * 4096)));
    }
}
