#include <rtl.h>
#include <mm.h>

#include <internal/ki.h>
#include <internal/mi.h>

void* HeapStart;
void* HeapEnd;
POOL_HEADER* LastHdr;

VOID
MmInitHeap(void* HeapAddress, UINT64 PageCount) {
    void* pos = HeapAddress;

    for (UINT64 i = 0; i < PageCount; i++){
		MmMapMemory(NULL, pos, MmRequestPage(), 1, PAGE_FLAGS_DEFAULT);
        pos = (void*)((UINT64)pos + 0x1000);
    }

    UINT64 HeapLength = PageCount * 0x1000;

    HeapStart = HeapAddress;
    HeapEnd = (void*)((UINT64)HeapStart + HeapLength);
    POOL_HEADER* startSeg = (POOL_HEADER*)HeapAddress;
    startSeg->len = HeapLength - sizeof(POOL_HEADER);
    startSeg->next = NULL;
    startSeg->last = NULL;
    startSeg->free = TRUE;
    LastHdr = startSeg;
}

VOID
MmReleasePool(void* Address) {
    POOL_HEADER* segment = (POOL_HEADER*)Address - 1;
    segment->free = TRUE;
	MiCombineForward(segment);
	MiCombineBackward(segment);
}

POOL_HEADER* MiSplitHeap(POOL_HEADER* Hdr, UINT64 splitLength) {
    if (splitLength < 0x10) return NULL;
    long splitSegLength = Hdr->len - splitLength - (sizeof(POOL_HEADER));
    if (splitSegLength < 0x10) return NULL;

    POOL_HEADER* newSplitHdr = (POOL_HEADER*) ((UINT64)Hdr + splitLength + sizeof(POOL_HEADER));
    Hdr->next->last = newSplitHdr;              // Set the next segment's last segment to our new segment
    newSplitHdr->next = Hdr->next;              // Set the new segment's next segment to out original next segment
    Hdr->next = newSplitHdr;                    // Set our new segment to the new segment
    newSplitHdr->last = Hdr;                    // Set our new segment's last segment to the current segment
    newSplitHdr->len = splitSegLength;          // Set the new header's length to the calculated value
    newSplitHdr->free = Hdr->free;              // make sure the new segment's free is the same as the original
    Hdr->len = splitLength;                     // set the length of the original segment to its new length

    newSplitHdr->Tag = 0;

    if (LastHdr == Hdr) LastHdr = newSplitHdr;
    return (POOL_HEADER*)newSplitHdr;
}

VOID* 
MmAllocatePool(UINT64 size, UINT64 Tag){
    if (size % 0x10 > 0){ // it is not a multiple of 0x10
        size -= (size % 0x10);
        size += 0x10;
    }

    if (size == 0) return NULL;

    POOL_HEADER* currentSeg = (POOL_HEADER*) HeapStart;
    while(TRUE){
        if(currentSeg->free){
            if (currentSeg->len > size){
                MiSplitHeap(currentSeg, size);
                currentSeg->free = FALSE;
				currentSeg->Tag = Tag;
                return (void*)((UINT64)currentSeg + sizeof(POOL_HEADER));
            }
            if (currentSeg->len == size){
                currentSeg->free = FALSE;
				currentSeg->Tag = Tag;
                return (void*)((UINT64)currentSeg + sizeof(POOL_HEADER));
            }
        }
        if (currentSeg->next == NULL) break;
        currentSeg = currentSeg->next;
    }
    MiExpandHeap(size);
    return MmAllocatePool(size, Tag);
}

VOID*
MmRelocatePool(UINT64 ptr, UINT64 size) {
	void* new = MmAllocatePool(size, ((POOL_HEADER*)ptr - sizeof(POOL_HEADER))->Tag);
	RtlCopyMem(new, ptr, size);
	MmReleasePool(ptr);
	return new;
}

VOID
MiExpandHeap(UINT64 length){
    if (length % 0x1000) {
        length -= length % 0x1000;
        length += 0x1000;
    }

    UINT64 pageCount = length / 0x1000;
    POOL_HEADER* newSegment = (POOL_HEADER*)HeapEnd;

    for (UINT64 i = 0; i < pageCount; i++){
		MmMapMemory(NULL, HeapEnd, MmRequestPage(), 1, PAGE_FLAGS_DEFAULT);
        HeapEnd = (void*)((UINT64)HeapEnd + 0x1000);
    }

    newSegment->free = TRUE;
    newSegment->last = LastHdr;
    LastHdr->next = newSegment;
    LastHdr = newSegment;
    newSegment->next = NULL;
    newSegment->len = length - sizeof(POOL_HEADER);
    MiCombineBackward(newSegment);
}

VOID
MiCombineForward(POOL_HEADER* Hdr) {
    if (Hdr->next == NULL) return;
    if (!Hdr->next->free) return;

    if (Hdr->next == LastHdr) LastHdr = Hdr;

    if (Hdr->next->next != NULL){
        Hdr->next->next->last = Hdr;
    }

    Hdr->len = Hdr->len + Hdr->next->len + sizeof(POOL_HEADER);
}

VOID
MiCombineBackward(POOL_HEADER* Hdr){
    if (Hdr->last != NULL && Hdr->last->free)
		MiCombineForward(Hdr->last);
}
