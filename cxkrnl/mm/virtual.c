#include <cxkrnl.h>
#include <rtl.h>
#include <internal/mi.h>

#include <boot.h>
#include <mm.h>
#include <kd.h>

#define MAKE_TABLE_ENTRY(address, flags) ((address & ~(0xfff)) | flags)

static ADDRESS_SPACE kspace;

static UINT64 *get_next_level(UINT64 *current_level, UINT64 entry) {
    UINT64 ret;

    if (current_level[entry] & 0x1) {
        ret = current_level[entry] & ~((UINT64)0xfff);
    } else {

        /* Allocate a table for the next paging level */
        ret = (UINT64)MmRequestPage();
        if (ret == 0) {
            return NULL;
        }
		RtlSetMem(ret, 0, 0x1000);
        current_level[entry] = ret | 0b111;
    }

    return (void *)ret;
}

VOID 
MiMapPage(ADDRESS_SPACE *pagemap, UINT64 physical_address, UINT64 virtual_address, UINT64 flags) {
    UINT64 level4 = (virtual_address & ((UINT64)0x1ff << 39)) >> 39;
    UINT64 level3 = (virtual_address & ((UINT64)0x1ff << 30)) >> 30;
    UINT64 level2 = (virtual_address & ((UINT64)0x1ff << 21)) >> 21;
    UINT64 level1 = (virtual_address & ((UINT64)0x1ff << 12)) >> 12;

    UINT64 *pml4, *pml3, *pml2, *pml1;

    pml4 = (void *)pagemap->PML4;

    pml3 = get_next_level(pml4, level4);

    pml2 = get_next_level(pml3, level3);

    pml1 = get_next_level(pml2, level2);

    pml1[level1] = physical_address | flags;
}

VOID
MiUnmapPage(ADDRESS_SPACE* addrspace, UINT64 vaddr) {
    UINT16 pte = (vaddr >> 12) & 0x1ff;
    UINT16 pde = (vaddr >> 21) & 0x1ff;
    UINT16 pdpe = (vaddr >> 30) & 0x1ff;
    UINT16 pml4e = (vaddr >> 39) & 0x1ff;

    UINT64* pml4 = addrspace->PML4;
    if (!(pml4[pml4e] & PAGE_PRESENT))
        return;

    UINT64* pdpt = (UINT64*)PHYS_TO_VIRT(pml4[pml4e] & ~(0x1ff));
    if (!(pdpt[pdpe] & PAGE_PRESENT))
        return;

    UINT64* pd = (UINT64*)PHYS_TO_VIRT(pdpt[pdpe] & ~(0x1ff));
    if (!(pd[pde] & PAGE_PRESENT))
        return;

    UINT64* pt = (UINT64*)PHYS_TO_VIRT(pd[pde] & ~(0x1ff));
    if (!(pt[pte] & PAGE_PRESENT))
        return;

    pt[pte] = 0;

    UINT64 cr3val = __readcr3();
    if (cr3val == (UINT64)(VIRT_TO_PHYS(addrspace->PML4)))
        __invlpg(vaddr);

    for (int i = 0; i < 512; i++)
        if (pt[i] != 0)
            goto done;
    pd[pde] = 0;
    MmFreePage(VIRT_TO_PHYS(pt));

    for (int i = 0; i < 512; i++)
        if (pd[i] != 0)
            goto done;
    pdpt[pdpe] = 0;
    MmFreePage(VIRT_TO_PHYS(pd));

    for (int i = 0; i < 512; i++)
        if (pdpt[i] != 0)
            goto done;
    pml4[pml4e] = 0;
    MmFreePage(VIRT_TO_PHYS(pdpt));

done:
    return;
}

VOID
MmUnmapMemory(ADDRESS_SPACE* addrspace, UINT64 vaddr, UINT64 np) {
    ADDRESS_SPACE* as = addrspace ? addrspace : &kspace;
    for (UINT64 i = 0; i < np * 0x1000; i += 0x1000)
        MiUnmapPage(as, vaddr + i);
}

VOID
MmMapMemory(ADDRESS_SPACE* addrspace, UINT64 vaddr, UINT64 paddr, UINT64 np, UINT64 flags) {
    ADDRESS_SPACE* as = addrspace ? addrspace : &kspace;
    for (UINT64 i = 0; i < np * 0x1000; i += 0x1000)
        MiMapPage(as, paddr + i, vaddr + i, flags);
}

VOID MmSyncMapping(IN LOADER_BLOCK* Block) {
	kspace.PML4 = MmRequestPage();
	EFI_MMAP* Mmap = Block->Mmap;
	UINT64 EntryCount = Block->MmapCount;
	UINT64 DescSize = Block->DescSize;
	
	RtlSetMem(kspace.PML4, 0, 0x1000);

    for (UINT64 i = 0; i < EntryCount; i++){
        EFI_MMAP* desc = (EFI_MMAP*)((UINT64)Mmap + (i * DescSize));
		MmMapMemory(NULL, desc->Physical, desc->Physical, desc->Pages, PAGE_FLAGS_DEFAULT);
    }

    // Enable Write-Combine Caching for the framebuffer
	MmMapMemory(NULL, Block->Fbuf.Base, Block->Fbuf.Base, RTL_NUM_TO_PAGES(Block->Fbuf.BSize), PAGE_FLAGS_DEFAULT | PAGE_WRITECOMBINE);
	INFO("MmSyncMapping(): Write-Combining the Framebuffer\n");

    __writecr3(VIRT_TO_PHYS(kspace.PML4));
}

