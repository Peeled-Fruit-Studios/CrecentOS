#include <cxkrnl.h>
#include <internal/ki.h>
#include <internal/mi.h>
#include <internal/halp.h>

#include <kd.h>

VOID
MmInitializeCaching() {
    IA32_PAT_MSR Pat;

    //
    // Write the default values to the PAT MSR
    // This fixes and bugs set by the bios
    // and makes sure they're set properly.
    //

    Pat.Long = KiMsrRead( IA32_MSR_PAT );
    Pat.Pa0 = MEM_TYPE_WB;
    Pat.Pa1 = MEM_TYPE_WC;
    Pat.Pa2 = MEM_TYPE_UC;
    KiMsrWrite( IA32_MSR_PAT, Pat.Long );

    //
    // This layout means 
    // pat=0,cd=0,wt=0, pa0
    // pat=0,cd=0,wt=1, pa1
    // pat=0,cd=1,wt=0, pa2
    //

    
    // Check for, and enable specific cpu features
    
    if ( KeProcessorFeatureEnabled( NULL, CPU_NX_ENABLED ) ) {

        KiMsrWrite( IA32_MSR_EFER, KiMsrRead( IA32_MSR_EFER ) | EFER_NXE );
    } else {
		LOG(LOG_WARNING, "NX is missing and is required for safe operation of CrecentOS\n");
	}

    if ( KeProcessorFeatureEnabled( NULL, CPU_PCID_ENABLED ) ) {

        KiCr4Write( KiCr4Read( ) | CR4_PCIDE );
    } else {
		LOG(LOG_WARNING, "PCID is missing and is required for safe operation of CrecentOS\n");
	}

    if ( KeProcessorFeatureEnabled( NULL, CPU_SMEP_ENABLED ) ) {

        KiCr4Write( KiCr4Read( ) | CR4_SMEP );
    } else {
		LOG(LOG_WARNING, "SMEP/SMAP is missing and is required for safe operation of CrecentOS\n");
	}
}

