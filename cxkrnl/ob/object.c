#include <ob.h>

#include <kd.h>

OBJECT_DIRECTORY Root;

VOID ObDumpNodes(OBJECT_DIRECTORY* Dir) {
	INFO("==== DUMPING NODES ====\n");
	INFO("Directory %s:\n", Dir->name);
	
	ObjectHeader* Buffer;
	for(int i = 0; i < Dir->DirCount; i++) {
		Buffer = ObGetChildByIndex(Dir, i);
		switch(Buffer->Type) {
			case OB_DIR:
			INFO("\t%s (Directory)\n", HDR2OBJ(Buffer, OBJECT_DIRECTORY)->name);
			break;
			case OB_DUMMY:
			INFO("\t(Message): %s\n", HDR2OBJ(Buffer, OBJECT_DUMMY)->Message);
			break;
			case OB_SYMLINK:
			INFO("\t%s -> %s (Symlink)\n", HDR2OBJ(Buffer, OBJECT_SYMBOLIC_LINK)->LinkName, HDR2OBJ(Buffer, OBJECT_SYMBOLIC_LINK)->LinkTargetName);
			break;
		}
	}
}

VOID ObInitializeObjectManager() {
	ObCreateDirectory("\\\\", &Root, 0, NULL);
	
	// Create the dummy object by hand to test the Object Manager
	OBJECT_DUMMY* Dmy = (OBJECT_DUMMY*)MmAllocatePool(sizeof(OBJECT_DUMMY), TAG_OB);
	*Dmy = (OBJECT_DUMMY) {
		.Header = (ObjectHeader) {
			.Type = OB_DUMMY,
			.Size = sizeof(OBJECT_DUMMY),
			.Permanant = FALSE,
			.RefCount = 1,
		},
		.Message = "Hello, World!",
	};
	
	ObAppendDirectory(&Root, Dmy);
	
	ObDumpNodes(&Root);
}
