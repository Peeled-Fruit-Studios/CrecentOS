#include <ob.h>

VOID
ObCreateDirectory(IN char* name, IN_OUT OBJECT_DIRECTORY* Dir, IN int Dcount, IN ObjectHeader* Children) {
	if(__strlen(name) > 128) {
		// Name too long
		return;
	}
	
	ObjectHeader hdr = Dir->Header;
	
	// Set up the header
	hdr = (ObjectHeader) {
		.Type = OB_DIR,
		.Size = sizeof(OBJECT_DIRECTORY),
		.Permanant = TRUE,
		.RefCount = 1,
	};
	
	Dir->Children = Children;
	Dir->DirCount = Dcount;
	Dir->DirMax = Dcount;
	
	RtlCopyMem(Dir->name, name, __strlen(name));
}

VOID
ObCreateSymLink(IN char* name, IN_OUT OBJECT_SYMBOLIC_LINK* SymLink, IN char* Target, IN ObjectHeader* TargetHeader) {
	ObjectHeader hdr = SymLink->Header;
	
	// Set up the header
	hdr = (ObjectHeader) {
		.Type = OB_SYMLINK,
		.Size = sizeof(OBJECT_SYMBOLIC_LINK),
		.Permanant = TRUE,
		.RefCount = 1,
	};
	
	*SymLink = (OBJECT_SYMBOLIC_LINK) {
		.LinkName = name,
		.LinkTargetName = Target,
		.LinkTarget = TargetHeader,
	};
}

