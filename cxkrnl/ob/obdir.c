#include <ob.h>
#include <mm.h>

VOID
ObAppendDirectory(IN_OUT OBJECT_DIRECTORY* Dir, IN ObjectHeader* Child) {
	if(Dir->Children == NULL) {
		Dir->Children = (ObjectHeader**)MmAllocatePool(sizeof(UINT64) * 2, TAG_OB);
		Dir->DirCount++;
		Dir->DirMax += 2; // Allocate one extra for safety
		
		Dir->Children[Dir->DirCount - 1] = Child;
	} else {
		if(Dir->DirCount >= Dir->DirMax) {
			Dir->Children = (ObjectHeader**)MmRelocatePool(Dir->Children, sizeof(UINT64) * (Dir->DirCount + 1));
			Dir->DirCount++;
			Dir->DirMax += 2;
			
			Dir->Children[Dir->DirCount - 1] = Child;
		} else {
			Dir->Children[Dir->DirCount++ - 1] = Child;
		}
	}
}

ObjectHeader*
ObGetChildByIndex(IN OBJECT_DIRECTORY* Obj, IN int index) {
	return Obj->Children[index];
}
